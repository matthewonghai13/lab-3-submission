// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPawn.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set this pawn to be controlled by the lowest-numbered player
    AutoPossessPlayer = EAutoReceiveInput::Player0;

    // Create a dummy root component we can attach things to.
    // RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    // Create a camera, visible component, and box collision

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	// BoxComponent->SetCollisionProfileName(TEXT("BlockAll"));
	RootComponent = BoxComponent;

    UCameraComponent* OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
    OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));
    
	
	// Attach our camera and visible object to our root component. Offset and rotate the camera.
	OurVisibleComponent->SetupAttachment(RootComponent);
    OurCamera->SetupAttachment(OurVisibleComponent);
    OurCamera->SetRelativeLocation(FVector(-250.0f, 0.0f, 250.0f));
    OurCamera->SetRelativeRotation(FRotator(-45.0f, 0.0f, 0.0f));

	MoveSpeed = 300.0f;

}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    // Handle movement based on our "MoveX" and "MoveY" axes
    {
        if (!CurrentVelocity.IsZero())
        {
            FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
            SetActorLocation(NewLocation);
        }
    }

}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAction("Interact", IE_Pressed, this, &AMyPawn::Interact);

    // Respond every frame to the values of our two movement axes, "MoveX" and "MoveY".
    InputComponent->BindAxis("MoveX", this, &AMyPawn::Move_XAxis);
    InputComponent->BindAxis("MoveY", this, &AMyPawn::Move_YAxis);
}

void AMyPawn::Move_XAxis(float AxisValue)
{
    // Move at 100 units per second forward or backward
    CurrentVelocity.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * MoveSpeed;
}

void AMyPawn::Move_YAxis(float AxisValue)
{
    // Move at 100 units per second right or left
    CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * MoveSpeed;
}

void AMyPawn::Interact() {
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("E Pressed"));

	TArray<AActor*>OverlappingActors;
	BoxComponent->GetOverlappingActors(OverlappingActors);

	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::FromInt(OverlappingActors.Num()) + "Overlapping Actors");

	for(AActor* actor : OverlappingActors) {
		if(actor->IsA(ACollisionActor::StaticClass())) {
			actor->Destroy();
		} else if(actor->IsA(ATriggerActor::StaticClass())) {
			((ATriggerActor*) actor)->OnTriggerDelegate.Broadcast();
			GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, FString::FromInt(OverlappingActors.Num()) + "Trigger Broadcasted");
		}
	}
}