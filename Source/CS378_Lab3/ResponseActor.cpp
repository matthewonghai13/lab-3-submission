// Fill out your copyright notice in the Description page of Project Settings.
#include "ResponseActor.h"

// Sets default values
AResponseActor::AResponseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	RootComponent = BoxComponent;
    OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));
	
	// Attach our camera and visible object to our root component. Offset and rotate the camera.
    OurVisibleComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AResponseActor::BeginPlay()
{
	Super::BeginPlay();

	TriggerActor->OnTriggerDelegate.AddDynamic(this, &AResponseActor::RespondToTrigger);
}

// Called every frame
void AResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AResponseActor::RespondToTrigger() {
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, "Trigger Received");
	SetActorLocation(FVector(582.0f, 150.0f, 20.0f), false, 0, ETeleportType::None);
}

