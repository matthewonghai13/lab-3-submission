// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Engine.h"
#include "Components/StaticMeshComponent.h"
#include "TriggerActor.h"
#include "ResponseActor.generated.h"

UCLASS()
class CS378_LAB3_API AResponseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResponseActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void RespondToTrigger();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	ATriggerActor* TriggerActor;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* OurVisibleComponent;

	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxComponent;
};
