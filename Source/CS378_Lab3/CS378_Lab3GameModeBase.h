// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine.h"
#include "MyPawn.h"
#include "CS378_Lab3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB3_API ACS378_Lab3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	// DefaultPawnClass = AMyPawn::StaticClass();	
};
